package az.employee.repository;

import az.employee.domain.Role;
import az.employee.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    User addUser(User user);
    Optional<User> getUserByEmail(String email);
    List<Role> getUserRoles(long userId);
}
