package az.employee.repository;

import az.employee.domain.Token;

import java.util.Optional;

public interface TokenRepository {

    Token addToken(Token token);

    Optional<Token> getToken(String token);
}
