package az.employee.repository.jdbc;

import az.employee.domain.*;
import az.employee.exception.ResourceDeleteException;
import az.employee.repository.CandidateRepository;
import az.employee.repository.jdbc.mapper.*;
import az.employee.util.ColumnType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CandidateRepositoryJdbcImpl implements CandidateRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private CandidateMapper candidateMapper;

    @Autowired
    private JobHistoryRowMapper jobHistoryRowMapper;

    @Autowired
    private EducationRowMapper educationRowMap;

    @Autowired
    private CertificateRowMapper certificateRowMapper;

    @Autowired
    private LanguageSkillRowMapper languageSkillRowMapper;

    @Autowired
    private SkillRowMapper skillRowMapper;

    @Autowired
    private TagsRowMapper tagsRowMapper;


    @Override
    public List<Candidate> getCandidateList(int offset, int pageSize) {
        String sql = SqlQuery.GET_CANDIDATE_LIST_WITH_PAGING.replace("{OFFSET}", String.valueOf(offset))
                .replace("{PAGE_SIZE}", String.valueOf(pageSize));

        return jdbcTemplate.query(sql, candidateMapper);
    }

    @Override
    public List<Candidate> getCandidateListAdmin(String search, int column, String columnDir, int start, int length) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("searchWord", "%" + search + "%");
        String sql = String.format(SqlQuery.GET_CANDIDATE_LIST_ADMIN, ColumnType.fromColumn(column), columnDir, start, length);
        return jdbcTemplate.query(sql, params, candidateMapper);
    }

    @Override
    public long getCandidateCount() {
        return jdbcTemplate.queryForObject(
                SqlQuery.GET_CANDIDATE_COUNT,
                new MapSqlParameterSource(),
                Long.class);
    }

    @Override
    public Candidate addCandidate(Candidate candidate) {

        return candidate;
    }

    @Override
    public Optional<Candidate> getCandidateById(long id) {
        Optional<Candidate> optionalCandidate = Optional.empty();

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        List<Candidate> list = jdbcTemplate.query(SqlQuery.GET_CANDIDATE_BY_ID, params, candidateMapper);
        if (!list.isEmpty()) {
            optionalCandidate = Optional.of(list.get(0));
        }
        return optionalCandidate;
    }


    @Override
    public List<JobHistory> getJobHistoryList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        return jdbcTemplate.query(SqlQuery.GET_CANDIDATE_JOB_HISTORY_LIST, params, jobHistoryRowMapper);
    }

    /*
    * String ADD_JOB_HISTORY = "insert into job_history(id, candidate_id, position, start_date, end_date, " +
            "   country_id, city_id, company, info) " +
            "values(null, :candidate_id, :position, :start_date, :end_date, " +
            "   :country_id, :city_id, :company, :info)";
    * */
    @Override
    public JobHistory addJobHistory(JobHistory jobHistory) {
        MapSqlParameterSource params =
                new MapSqlParameterSource("candidate_id", jobHistory.getCandidateId())
                        .addValue("position", jobHistory.getPosition())
                        .addValue("company", jobHistory.getCompany())
                        .addValue("start_date", jobHistory.getStartDate())
                        .addValue("end_date", jobHistory.getEndDate())
                        .addValue("country_id", jobHistory.getCountry().getId())
                        .addValue("city_id", jobHistory.getCity().getId())
                        .addValue("info", jobHistory.getInfo());

        KeyHolder keyHolder = new GeneratedKeyHolder();
        int count = jdbcTemplate.update(SqlQuery.ADD_JOB_HISTORY, params, keyHolder);
        if (count > 0) {
            jobHistory.setId(keyHolder.getKey().longValue());
        } else {
            throw new RuntimeException("Job history not added, count = 0");
        }

        return jobHistory;
    }

    @Override
    public Optional<JobHistory> getJobHistoryById(long id) {
        Optional<JobHistory> optionalJobHistory = Optional.empty();

        MapSqlParameterSource params = new MapSqlParameterSource("job_history_id", id);
        List<JobHistory> jobHistoryList = jdbcTemplate.query(SqlQuery.GET_JOB_HISTORY_BY_ID, params, jobHistoryRowMapper);
        if (!jobHistoryList.isEmpty()) {
            optionalJobHistory = Optional.of(jobHistoryList.get(0));
        }
        return optionalJobHistory;
    }

    @Override
    public JobHistory updateJobHistory(JobHistory jobHistory) {
        MapSqlParameterSource params =
                new MapSqlParameterSource("start_date", jobHistory.getStartDate())
                        .addValue("end_date", jobHistory.getEndDate())
                        .addValue("company", jobHistory.getCompany())
                        .addValue("position", jobHistory.getPosition())
                        .addValue("info", jobHistory.getInfo())
                        .addValue("country_id", jobHistory.getCountry().getId())
                        .addValue("city_id", jobHistory.getCity().getId());

        /*
        *  public static String UPDATE_JOB_HISTORY_BY_ID = "update job_history" +
            "set start_date = :start, end_date = :end_date, " +
            "   company = :company, position = :position, " +
            "   country_id = :country_id, city_id = :city_id, " +
            "   info = :info " +
            "where id = :job_history_id and status = 1";
        * */
        int count = jdbcTemplate.update(SqlQuery.UPDATE_JOB_HISTORY_BY_ID, params);
        if(count > 0) {
            // todo add logging
        } else {
            throw new RuntimeException("Job History with id " + jobHistory.getId() + " not updated");
        }
        return jobHistory;
    }

    @Override
    public void deleteJobHistory(long id) {
        MapSqlParameterSource parameterSource = new MapSqlParameterSource("job_history_id", id);
        int count = jdbcTemplate.update(SqlQuery.DELETE_JOB_HISTORY_BY_ID, parameterSource);
        if(count == 0) {
            throw new ResourceDeleteException("Job history with id " + id + " not deleted", id);
        }
    }

    @Override
    public List<Education> getEducationList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        return jdbcTemplate.query(SqlQuery.GET_EDUCATIONS_LIST, params, educationRowMap);
    }

    @Override
    public List<Certificate> getCertificateList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        return jdbcTemplate.query(SqlQuery.GET_CERTIFICATE_LIST, params, certificateRowMapper);
    }

    @Override
    public List<LanguageSkill> getLanguageSkillList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        return jdbcTemplate.query(SqlQuery.GET_LANGUAGES_SKILLS, params, languageSkillRowMapper);
    }

    @Override
    public List<Skill> getSkillList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        return jdbcTemplate.query(SqlQuery.GET_CANDIDATE_SKILL_LIST, params, skillRowMapper);
    }

    @Override
    public List<Tag> getTagList(long id) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("candidate_id", id);
        return jdbcTemplate.query(SqlQuery.GET_CANDIDATE_TAGS, params, tagsRowMapper);
    }

    @Override
    public void saveProfileImage(long candidateId, String image) {
        MapSqlParameterSource params = new MapSqlParameterSource("image", image)
                .addValue("id", candidateId);

        int count = jdbcTemplate.update(SqlQuery.SAVE_CANDIDATE_PROFILE_IMAGE, params);
        if(count == 0) {
            throw new RuntimeException("Error updating candidate profile image, id = " + candidateId);
        }
    }
}
