package az.employee.repository.jdbc.mapper;

import az.employee.domain.JobUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class JobUserRowMapper implements RowMapper<JobUser> {
    @Override
    public JobUser mapRow(ResultSet rs, int rowNum) throws SQLException {
/*
        "select u.id user_id, " +
                " u.name, u.surname, u.email, " +
                " j.position, j.id job_id, c.profile_image " +
                " from job_user ju join job j on ju.job_id=j.id and ju.status = 1 " +
                " join user u on ju.user_id=u.id and u.status = 1\n" +
                " join candidate c on u.id=c.user_id and c.status = 1\n" +
                " limit {OFFSET}, {PAGE_SIZE}";*/

        JobUser jobUser = new JobUser();

        if (rs.getLong("user_id") > 0) {
            jobUser.getUser().setId(rs.getLong("user_id"));
            jobUser.getUser().setName(rs.getString("name"));
            jobUser.getUser().setSurname(rs.getString("surname"));
            jobUser.getUser().setEmail(rs.getString("email"));
        }
        if(rs.getLong("job_id")>0) {
            jobUser.getJob().setId(rs.getLong("job_id"));
            jobUser.getJob().setPosition(rs.getString("position"));
        }
        if(rs.getLong("candidate_id")>0) {
            jobUser.getCandidate().setId(rs.getLong("candidate_id"));
            jobUser.getCandidate().setProfileImage(rs.getString("profile_image"));
        }


        return jobUser;
    }
}
