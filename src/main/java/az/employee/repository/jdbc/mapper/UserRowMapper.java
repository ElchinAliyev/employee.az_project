package az.employee.repository.jdbc.mapper;

import az.employee.domain.User;
import az.employee.domain.UserStatus;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
        User user = new User();

        //GET_USER_BY_EMAIL = "select id, name, surname, contact_id, user_status_id, " +
//            "   email, password, idate, udate " +
//            " from user " +
//            "where email = :email and status = 1";
        user.setId(rs.getLong("id"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        // todo contact
        user.setStatus(UserStatus.fromValue(rs.getInt("user_status_id")));
        user.setPhone(rs.getString("phone"));
        user.setMobile(rs.getString("mobile"));
        user.setEmail(rs.getString("email"));
        user.setPassword(rs.getString("password"));
        user.setInsertDate(rs.getTimestamp("idate").toLocalDateTime());
        if(rs.getTimestamp("udate") != null) {
            user.setLastUpdate(rs.getTimestamp("udate").toLocalDateTime());
        }
        return user;
    }
}
