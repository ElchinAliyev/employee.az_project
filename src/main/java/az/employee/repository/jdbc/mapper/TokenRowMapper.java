package az.employee.repository.jdbc.mapper;

import az.employee.domain.Token;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TokenRowMapper implements RowMapper<Token> {

    @Override
    public Token mapRow(ResultSet rs, int rowNum) throws SQLException {
        Token tokenObj = new Token();
        tokenObj.setId(rs.getLong("id"));
        tokenObj.setValue(rs.getString("value"));
        tokenObj.setGenerationDate(rs.getTimestamp("generation_date").toLocalDateTime());
        tokenObj.setExpireDate(rs.getTimestamp("expire_date").toLocalDateTime());
        tokenObj.setUsed(rs.getInt("used") == 1);
        tokenObj.getUser().setId(rs.getLong("user_id"));
        tokenObj.setInsertDate(rs.getTimestamp("idate").toLocalDateTime());
        if(rs.getTimestamp("udate") != null) {
            tokenObj.setLastUpdate(rs.getTimestamp("udate").toLocalDateTime());
        }
        return tokenObj;
    }
}
