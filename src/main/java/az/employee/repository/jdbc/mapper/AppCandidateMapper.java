package az.employee.repository.jdbc.mapper;

import az.employee.domain.Candidate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AppCandidateMapper implements RowMapper<Candidate> {
    @Override
    public Candidate mapRow(ResultSet rs, int rowNum) throws SQLException {

    /*    "select u.id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
                "ct.name cityName, cr.name countryName, a.info" +
                "from user u join candidate c on u.id=c.user_id and u.status =1 and c.status=1\n" +
                "join address a on a.id=c.address_id and a.status=1\n" +
                "join city ct on a.city_id=ct.id and ct.status=1\n" +
                "join country cr on a.country_id = cr.id and cr.status=1\n" +
                "where u.id in(\n" +
                "   select user_id\n" +
                "   from job_user\n" +
                "   where job_id in(\n" +
                "      select id\n" +
                "      from job\n" +
                "      where user_id=:appUserId\n" +
                "   )\n" +
                ")\n";*/

        Candidate candidate = new Candidate();

        if(rs.getLong("id") > 0) {
            candidate.getUser().setId(rs.getLong("id"));
            candidate.getUser().setName(rs.getString("name"));
            candidate.getUser().setSurname(rs.getString("surname"));
            candidate.getUser().setEmail(rs.getString("email"));
            if(rs.getString("phone") !=null) {
                candidate.getUser().setPhone(rs.getString("phone"));
            }
            if(rs.getString("mobile") !=null) {
                candidate.getUser().setMobile(rs.getString("mobile"));
            }
        }
        candidate.getAddress().getCity().setName(rs.getString("cityName"));
        candidate.getAddress().getCountry().setName(rs.getString("countryName"));
        if(rs.getString("info") !=null) {
            candidate.getAddress().setInfo(rs.getString("info"));
        }
        candidate.setId(rs.getLong("candidateId"));

        return candidate;
    }
}
