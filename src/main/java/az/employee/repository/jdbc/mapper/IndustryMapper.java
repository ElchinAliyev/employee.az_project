package az.employee.repository.jdbc.mapper;

import az.employee.domain.Industry;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class IndustryMapper implements RowMapper<Industry> {
    @Override
    public Industry mapRow(ResultSet resultSet, int i) throws SQLException {
        Industry industry = new Industry();
        industry.setId(resultSet.getLong("id"));
        industry.setName(resultSet.getString("name"));
        return industry;
    }
}
