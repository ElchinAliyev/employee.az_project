package az.employee.repository.jdbc.mapper;

import az.employee.domain.Company;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

@Component
public class CompanyMapper implements RowMapper<Optional<Company>> {
    /*
    *
    *
    * " select c.id, c.name , c.head_office , c.about, c.create_date , c.num_of_employee , c.annual_revenue,\n" +
            " c.is_global,c.rating, c.phone , c.mobile , c.email , c.website \n" +
            " from company_user cu\n" +
            " join user u on u.id = cu.user_id and u.id = :userId and u.status = 1 \n" +
            " join company c on c.id=cu.organization_id and c.status = 1 ";

    * */

    @Override
    public Optional<Company> mapRow(ResultSet resultSet, int i) throws SQLException {
        Optional<Company> optionalCompany = Optional.empty();
        Company company =  new Company();
        company.setId(resultSet.getLong("id"));
        company.setName(resultSet.getString("name"));
        company.setEmail(resultSet.getString("email"));
        company.setMobile(resultSet.getString("mobile"));
        company.setPhone(resultSet.getString("phone"));
        company.setAbout(resultSet.getString("about"));
        company.setAnnualRevenue(resultSet.getBigDecimal("annual_revenue"));
        company.setGlobal(resultSet.getInt("is_global")==1);
        company.setHeadOffice(resultSet.getString("head_office"));
        if (resultSet.getLong("industry_id")>0){
            company.getIndustry().setId(resultSet.getLong("industry_id"));
        }
        company.setWebsite(resultSet.getString("website"));
        company.setRating(resultSet.getLong("rating"));
        company.setNumberOfEmployees(resultSet.getLong("num_of_employee"));
        company.setCreateDate(resultSet.getString("create_date"));
        optionalCompany = Optional.of(company);
        return optionalCompany;
    }
}
