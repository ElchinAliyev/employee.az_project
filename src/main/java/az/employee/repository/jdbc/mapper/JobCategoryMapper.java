package az.employee.repository.jdbc.mapper;

import az.employee.domain.JobCategory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class JobCategoryMapper implements RowMapper<JobCategory> {
    @Override
    public JobCategory mapRow(ResultSet resultSet, int i) throws SQLException {
        JobCategory jobCategory = new JobCategory();
        jobCategory.setId(resultSet.getLong("id"));
        jobCategory.setName(resultSet.getString("name"));
        return jobCategory;
    }
}
