package az.employee.repository.jdbc;

import az.employee.domain.*;
import az.employee.repository.CompanyRepository;
import az.employee.repository.jdbc.mapper.*;
import az.employee.util.CompanyJobDataTableColumnNames;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Repository
public class CompanyRepositoryImpl implements CompanyRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private CountryMapper countryMapper;
    @Autowired
    private JobTypeMapper jobTypeMapper;
    @Autowired
    private JobCategoryMapper jobCategoryMapper;
    @Autowired
    private CityMapper cityMapper;
    @Autowired
    private CompanyJobAjaxMapper companyJobAjaxMapper;
    @Autowired
    private IndustryMapper industryMapper;
    @Autowired
    private  AppCandidateMapper appCandidateMapper;

  @Autowired
  private JobUserRowMapper jobUserRowMapper;
    /*
    * " select c.id, c.name , c.head_office , c.about, c.create_date , c.num_of_employee , c.annual_revenue,\n" +
            " c.is_global,c.rating, c.phone , c.mobile , c.email , c.website \n" +
            " from company_user cu\n" +
            " join user u on u.id = cu.user_id and u.id = :userId and u.status = 1 \n" +
            " join company c on c.id=cu.organization_id and c.status = 1 ";
    *
    *
    *
    * */
    @Override
    public Optional<Company> getCompanyByUserId(long userId) {
        Optional<Company> optionalCompany = Optional.empty();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("userId",userId);
        optionalCompany = jdbcTemplate.queryForObject(SqlQuery.GET_COMPANY_BY_USER_ID,sqlParameterSource,companyMapper);
        return optionalCompany;
    }
/*
* "select count(id) from job where company_id = :companyId "
*
*
*
* */
    @Override
    public long getCompanyJobCount(long id) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("companyId",id);

        return  jdbcTemplate.queryForObject(SqlQuery.GET_COMPANY_JOB_COUNT_BY_COMPANY_ID,sqlParameterSource,Long.class);
    }
/*
*
*
*
*
*  " select j.position , j.about , j.requirement , j.salary_min , j.salary_max , j.post_date , j.deadline , cr.name , ci.name , a.info ,jt.name,\n" +
            " j.experience_min,j.experience_max , jc.name , jcp.name , com.name\n" +
            " from job j\n" +
            " join company com on com.id = j.company_id\n" +
            " join job_category jc on jc.id = j.job_category_id\n" +
            " join job_category jcp on jcp.id = jc.parent_id\n" +
            " join job_type jt on jt.id = j.job_type_id\n" +
            " join address a on j.address_id = a.id and  j.company_id = :companyId \n" +
            " join country cr on cr.id = a.country_id\n" +
            " join city ci on ci.id = a.city_id\n" +
            " Limit {OFFSET} , {LENGTH} " ;
*
*
*
*
*
* */
    @Override
    public List<Job> getCompanyJobListByPage(long offset, long length, long companyId) {
        System.out.println("Offset " + offset);
        System.out.println("length " + length);
        System.out.println("companyId " + companyId);
        String sql = SqlQuery.GET_COMPANY_JOB_LIST_BY_PAGE.replace("{OFFSET}",String.valueOf(offset))
                .replace("{LENGTH}",String.valueOf(length));
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("companyId",companyId);

      List<Job> list =jdbcTemplate.query(sql,sqlParameterSource,jobMapper);
        System.out.println("Bazandan Gelen JobList " + list);
        return list;
    }
/*
" insert into address(id,country_id,city_id,info) " +
           " Values (null,:countryId,:cityId,:info); ";

* "Insert into job(user_id ,salary_min ,salary_max," +
            " deadline,experience_min,experience_max,job_category_id, " +
            " address_id, position , about , requirement , job_type_id,company_id) " +
            "Values ( :userId,:salaryMin,:salaryMax, :deadline, :experienceMin , :experienceMax ,:jobCategoryId, " +
            " :addressId ,:position,:about,:requirement , :jobTypeId , :companyId) ";
*
* */


    @Override
    public void insertJob(Job job) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("countryId",job.getAddress().getCountry().getId())
                .addValue("cityId",job.getAddress().getCity().getId())
                .addValue("info",job.getAddress().getInfo());
        KeyHolder addressKeyHolder = new GeneratedKeyHolder();
     int count =   jdbcTemplate.update(SqlQuery.INSERT_ADDRESS,sqlParameterSource,addressKeyHolder);

     if (count>0){

         SqlParameterSource parameterSource = new MapSqlParameterSource()
                 .addValue("userId",job.getUser().getId())
                 .addValue("position",job.getPosition())
                 .addValue("about",job.getAbout())
                 .addValue("requirement",job.getRequirement())
                 .addValue("jobTypeId",job.getType().getId())
                 .addValue("companyId",job.getCompany().getId())
                 .addValue("salaryMin",job.getMinSalary())
                 .addValue("salaryMax",job.getMaxSalary())
                 .addValue("experienceMin",job.getMinExperience())
                 .addValue("experienceMax",job.getMaxExperience())
                 .addValue("addressId",addressKeyHolder.getKey().longValue())
                 .addValue("deadline",job.getDeadline())
                 .addValue("jobCategoryId",job.getCategory().getId());
         KeyHolder jobIdHolder = new GeneratedKeyHolder();
       count =  jdbcTemplate.update(SqlQuery.INSERT_JOB,parameterSource,jobIdHolder);
       if (count > 0){
           System.out.println(jobIdHolder.getKey().longValue() + " idli job insert edilmisdir!");
       }
       else {
           throw  new RuntimeException("Job Insert edile bilmedi ! ");
       }


     }
     else {
         throw  new RuntimeException("Address insert edilemedi !");
     }






    }

    @Override
    public List<Country> getCountryList() {
       return jdbcTemplate.query(SqlQuery.GET_COUNTRY_LIST,new MapSqlParameterSource(),countryMapper);
    }

    @Override
    public List<City> getCityList() {
        return jdbcTemplate.query(SqlQuery.GET_CITY_LIST, new MapSqlParameterSource(),cityMapper);
    }

    @Override
    public List<JobType> getJobTypeList() {
        return  jdbcTemplate.query(SqlQuery.GET_JOB_TYPE_LIST,new MapSqlParameterSource(),jobTypeMapper);
    }

    @Override
    public List<JobCategory> getJobCategoryList() {
        return  jdbcTemplate.query(SqlQuery.GET_JOB_CATEGORY,new MapSqlParameterSource(),jobCategoryMapper);
    }
/*
* " select concat(u.name ,\" \", u.surname) as editor , j.position , j.about,jc.name job_category_name ,requirement , j.deadline,\n" +
           "concat(c.name,\" \",cy.name,\" \", a.info) as Address from job j " +
           "join user u on u.id = j.user_id\n" +
           "join address a on a.id = j.address_id\n" +
           "join country c on c.id = a.country_id and c.id = :companyId" +
           "join city cy on cy.id = a.city_id\n" +
           "join job_category jc on jc.id = j.job_category_id\n" +
           "where concat(concat(u.name ,\" \", u.surname),j.position,j.about,jc.name,requirement,j.deadline) Like :condition" +
           "order by {column_name} {direction} " +
           "Limit {offset} , {length}  ";
* */
    @Override
    public List<Job> getCompanyJobListWithAjax(DataTableRequest dataTableRequest,long companyId) {
        String sql = SqlQuery.GET_COMPANY_JOB_WITH_AJAX
                .replace("{column_name}", String.valueOf(CompanyJobDataTableColumnNames.whichColumn(dataTableRequest.getSortColumn())))
                .replace("{direction}",dataTableRequest.getSortDirection())
                .replace("{offset}",String.valueOf(dataTableRequest.getStart()))
                .replace("{length}",String.valueOf(dataTableRequest.getLength()));

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("companyId",companyId)
                .addValue("condition","%"+dataTableRequest.getFilter()+"%");

     return  jdbcTemplate.query(sql,sqlParameterSource,companyJobAjaxMapper);

    }
    /*
    * " select count(j.position) from job j " +
           "join user u on u.id = j.user_id\n and j.company_id = :companyId " +
           "join address a on a.id = j.address_id\n" +
           "join country c on c.id = a.country_id " +
           "join city cy on cy.id = a.city_id\n" +
           "join job_category jc on jc.id = j.job_category_id " +
           "where concat(concat(u.name ,\" \", u.surname),j.position,j.about,jc.name,requirement,j.deadline ,concat(c.name,\" \",cy.name,\" \", a.info)) Like :condition " ;
    *
    * */

    @Override
    public long getFilteredCompanyJobCount(String searchValue,long companyId) {

        return jdbcTemplate.queryForObject(SqlQuery.GET_FILTERED_COMPANY_JOB_COUNT,new MapSqlParameterSource().addValue("companyId",companyId)
                .addValue("condition","%"+searchValue+"%")
                ,Long.class);

    }
/*
" select j.id , j.position , j.about , j.requirement , j.salary_min , j.address_id , a.country_id , a.city_id," +
           " j.salary_max , j.post_date , j.deadline , cr.name country_name , ci.name city_name , j.job_type_id , a.info ,jt.name job_type_name,\n" +
           " j.experience_min,j.experience_max , j.job_category_id , jcp.id parent_job_cat_id , jc.name job_category_name , jcp.name parent_job_cat_name ," +
           " o.name company_name\n" +
           ",j.user_id, u.name, u.surname, u.email, u.phone, u.mobile,\n" +
           "  o.id company_id , o.email company_email, o.phone company_phone, o.mobile company_mobile, \n" +
           "  j.idate, j.udate\n " +
           " from job j\n" +
           " join user u on u.id = j.user_id and j.id = :jobId "+
           " join company o on o.id = j.company_id\n" +
           " join job_category jc on jc.id = j.job_category_id\n" +
           " join job_category jcp on jcp.id = jc.parent_id\n" +
           " join job_type jt on jt.id = j.job_type_id\n" +
           " join address a on j.address_id = a.id and  j.company_id = :companyId \n" +
           " join country cr on cr.id = a.country_id\n" +
           " join city ci on ci.id = a.city_id " ;



 */
    @Override
    public List<Job> getJobByCompanyIdAndJobId(long jobId, long companyId) {

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("jobId",jobId)
                .addValue("companyId",companyId);

     List<Job> jobList = jdbcTemplate.query(SqlQuery.GET_SINGLE_JOB_BY_COMPANY_ID_AND_JOB_ID,sqlParameterSource,jobMapper);
        return jobList;
    }
/*
*
* "update job set position = :position , about = :about ,requirement = :requirement,salary_min=:salaryMin,salary_max = :salaryMax,deadline = :deadline,\n" +
           " job_type_id = :jobTypeId , experience_min = :experienceMin , experience_max = :experienceMax ,  job_category_id = :jobCategoryId where id = :jobId ";
*
*
* "update address set country_id = :countryId , city_id = :cityId , info = :info where id = :addressId ";
* */
    @Override
    public void editVacancyByJob(Job job) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("countryId",job.getAddress().getCountry().getId())
                .addValue("cityId",job.getAddress().getCity().getId())
                .addValue("info",job.getAddress().getInfo())
                .addValue("addressId",job.getAddress().getId());
      int count =  jdbcTemplate.update(SqlQuery.UPDATE_ADDRESS,sqlParameterSource);
      if  (count>0){
          System.out.println("counta  girdi");
          SqlParameterSource parameterSource = new MapSqlParameterSource()
                  .addValue("position",job.getPosition())
                  .addValue("about",job.getAbout())
                  .addValue("requirement",job.getRequirement())
                  .addValue("salaryMin",job.getMinSalary())
                  .addValue("salaryMax",job.getMaxSalary())
                  .addValue("deadline",job.getDeadline())
                  .addValue("jobTypeId",job.getType().getId())
                  .addValue("experienceMin",job.getMinExperience())
                  .addValue("experienceMax",job.getMaxExperience())
                  .addValue("jobCategoryId",job.getCategory().getId())
                  .addValue("jobId",job.getId());
        count =  jdbcTemplate.update(SqlQuery.UPDATE_JOB,parameterSource);
        if (count > 0){

            System.out.println("Job Ugurla yenilendi");
        }
        else {
            throw new RuntimeException(job.getId() + " update edilemedi !");
        }
      }
      else{
          throw new RuntimeException(job.getAddress().getId() + " update edilemedi !");
      }


    }

    @Override
    public List<Industry> getIndustryList() {

      return  jdbcTemplate.query(SqlQuery.GET_INDUSTRY_LIST , new MapSqlParameterSource() ,industryMapper);

    }
/*
* "select c.id , c.name , c.industry_id,c.head_office,c.create_date,c.num_of_employee,\n" +
        "c.annual_revenue,c.is_global , c.rating,c.about,c.phone,c.mobile,c.website,c.email from company c \n" +
        "join industry i on i.id = c.industry_id and c.id = :id "
* */
    @Override
    public List<Company> getSingleCompanyByCompanyId(long companyId) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id",companyId);
     List<Optional<Company>> companyList =   jdbcTemplate.query(SqlQuery.GET_COMPANY_BY_COMPANY_ID,sqlParameterSource,companyMapper);

     List<Company> companyList1 = new ArrayList<>();
     companyList1.add(companyList.get(0).get());
     return companyList1;
    }
/*
* "update company set name = :name , industry_id = :industryId , head_office = :headOffice , create_date = :createDate , num_of_employee = :numOfEmployee,\n" +
        "annual_revenue = :annualRevenue , is_global = :isGlobal , rating = :rating , about = :about , phone = :phone , mobile=:mobile , website = :website,\n" +
        "email = :email where id = :id   ";
*
*
* */
    @Override
    public void updateCompany(Company company) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("name",company.getName())
                .addValue("industryId",company.getIndustry().getId())
                .addValue("headOffice",company.getHeadOffice())
                .addValue("createDate",company.getCreateDate())
                .addValue("numOfEmployee",company.getNumberOfEmployees())
                .addValue("annualRevenue",company.getAnnualRevenue())
                .addValue("rating",company.getRating())
                .addValue("about",company.getAbout())
                .addValue("phone",company.getPhone())
                .addValue("mobile",company.getMobile())
                .addValue("website",company.getWebsite())
                .addValue("email",company.getEmail())
                .addValue("id",company.getId());


        if (company.isGlobal()){
            mapSqlParameterSource.addValue("isGlobal",1);
        }
        else {
            mapSqlParameterSource.addValue("isGlobal",0);
        }

     int count =   jdbcTemplate.update(SqlQuery.UPDATE_COMPANY_BY_ID,mapSqlParameterSource);
        if (count > 0 ){
            System.out.println(company.getId() + " ugurla update olundu ");
        }
        else {
            throw new RuntimeException(company.getId() + " yenilenmesinde prablem bas verdi !");
        }

    }

    /*
    * "update job set status = 0 where id =:id " +
        " and company_id = :companyId  ";
    * */

    @Override
    public void deleteJobByIdAndCompanyId(long jobId, long companyId) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("id",jobId)
                .addValue("companyId",companyId);

       int count = jdbcTemplate.update(SqlQuery.DELETE_JOB_BY_ID_AND_COMPANY_ID,sqlParameterSource);

       if (count>0){
           System.out.println(jobId + " idli is silindi !");
       }
       else {
           throw new RuntimeException(jobId + " idli is silinemedi !");
       }

    }

    @Override
    public List<JobUser> getCandidateNJobList(int offset, int jobUserpageSize, long userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("userId", userId);
        String sql = SqlQuery.GET_CANDIDATE_N_JOB_LIST_WITH_PAGING.replace("{OFFSET}", String.valueOf(offset))
                .replace("{PAGE_SIZE}", String.valueOf(jobUserpageSize));

        return jdbcTemplate.query(sql, params, jobUserRowMapper);
    }

    @Override
    public long getCandidateNJobPageCount(long userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("userId", userId);

        return jdbcTemplate.queryForObject(
                SqlQuery.GET_JOB_USER_COUNT,
                params,
                Long.class);
    }

    @Override
    public long CanAppComforAjaxCount(long userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("userId", userId);

        return jdbcTemplate.queryForObject(
                SqlQuery.CAN_APP_COM_FOR_AJAX_COUNT,
                params,
                Long.class);
    }

    @Override
    public long CanAppComforAjaxFilteredCount(long userId, String filter) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("userId", userId)
                .addValue("filter","%" + filter + "%");

        return jdbcTemplate.queryForObject(
                SqlQuery.CAN_APP_COM_FOR_AJAX_FILTERED_COUNT,
                params,
                Long.class);
    }


    @Override
    public List<Candidate> getCanAppList(
            long userId, int start,
            int length, String filter,
            Map<Integer,String> columnMap,
           DataTableRequest request
    ) {

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("userId", userId)
                .addValue("filter","%" + filter + "%");

        String sql = SqlQuery.GET_APP_CANDIDATE_LIST_PAGING.replace("{SORT_COLUMN}",columnMap.get(request.getSortColumn()))
                .replace("{SORT_DIRECTION}", request.getSortDirection())
                .replace("{START}", String.valueOf(start))
                .replace("{LENGTH}", String.valueOf(length));

        return jdbcTemplate.query(sql, params, appCandidateMapper);
    }

    public void deleteCandidateAllApps(long userId) {
        int count = jdbcTemplate.update(SqlQuery.DELETE_CANDIDATE_ALL_APPS,
                new MapSqlParameterSource("userId",userId));

        if (count==0){
            throw new RuntimeException("candidate applications has not deleted");
        }
    }
    @Override
    public long getIdFromJobUser(long jobId, long userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("userId", userId).
                        addValue("jobId",jobId);

        return jdbcTemplate.queryForObject(
                SqlQuery.GET_ID_FROM_JOB_USER,
                params,
                Long.class);
    }

    public void deleteCandidateApp(long id) {
        int count = jdbcTemplate.update(SqlQuery.DELETE_CANDIDATE_APP,
                new MapSqlParameterSource("id",id));

        if (count==0){
            throw new RuntimeException("Error deleting candidate app");
        }
    }
}
