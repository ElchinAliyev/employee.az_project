package az.employee.repository.jdbc;

import az.employee.domain.City;
import az.employee.domain.Country;
import az.employee.repository.CommonRepository;
import az.employee.repository.jdbc.mapper.CityMapper;
import az.employee.repository.jdbc.mapper.CountryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CommonRepositoryImpl implements CommonRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private CountryMapper countryMapper;

    @Autowired
    private CityMapper cityMapper;


    @Override
    public List<Country> getCountryList() {
        return jdbcTemplate.query(SqlQuery.GET_COUNTRY_LIST, new MapSqlParameterSource(), countryMapper);
    }

    @Override
    public List<City> getCityList(long countryId) {
        return jdbcTemplate.query(SqlQuery.GET_CITY_LIST_BY_COUNTRY_ID,
                new MapSqlParameterSource("country_id", countryId), cityMapper);
    }
}
