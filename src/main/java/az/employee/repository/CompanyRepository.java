package az.employee.repository;

import az.employee.domain.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface CompanyRepository {
    Optional<Company> getCompanyByUserId(long userId);
    long getCompanyJobCount(long id);
    List<Job> getCompanyJobListByPage(long offset, long length, long companyId);
    void insertJob(Job job);
    List<Country> getCountryList();
    List<City> getCityList();
    List<JobType> getJobTypeList();
    List<JobCategory> getJobCategoryList();
    List<Job> getCompanyJobListWithAjax(DataTableRequest dataTableRequest, long companyId);
    long getFilteredCompanyJobCount(String searchValue, long companyId);
    List<Job> getJobByCompanyIdAndJobId(long jobId, long companyId);
    void editVacancyByJob(Job job);
    List<Industry> getIndustryList();
    List<Company> getSingleCompanyByCompanyId(long companyId);
    void updateCompany(Company company);
    void deleteJobByIdAndCompanyId(long jobId, long companyId);
    List<JobUser> getCandidateNJobList(int offset, int jobUserpageSize, long userId);
    long getCandidateNJobPageCount(long userId);
    long getIdFromJobUser(long jobId, long userId);
    void deleteCandidateApp(long id);
    long CanAppComforAjaxCount(long userId);
    long CanAppComforAjaxFilteredCount(long userId, String filter);
    List<Candidate> getCanAppList(long userId, int start, int length,
                                  String filter, Map<Integer, String> columnMap,
                                  DataTableRequest request);
    void deleteCandidateAllApps(long userId);
}
