package az.employee.repository;

import az.employee.domain.*;
import az.employee.domain.*;

import java.util.List;
import java.util.Optional;

public interface CandidateRepository {

    List<Candidate> getCandidateListAdmin(String search, int column, String columnDir, int start, int length);
    List<Candidate> getCandidateList(int offset, int pageSize);
    long getCandidateCount();
    Optional<Candidate> getCandidateById(long id);

    Candidate addCandidate(Candidate candidate);

    // Job History
    List<JobHistory> getJobHistoryList(long id);
    JobHistory addJobHistory(JobHistory jobHistory);
    Optional<JobHistory> getJobHistoryById(long id);
    JobHistory updateJobHistory(JobHistory jobHistory);
    void deleteJobHistory(long id);

    List<Education> getEducationList(long id);
    List<Certificate> getCertificateList(long id);
    List<LanguageSkill> getLanguageSkillList(long id);
    List<Skill> getSkillList(long id);
    List<Tag> getTagList(long id);

    void saveProfileImage(long candidateId, String image);

}
