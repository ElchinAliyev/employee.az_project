package az.employee.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.math.BigDecimal;

@RequestMapping("/hello")
@Controller
public class HelloController {

    @GetMapping("/")
    public String index() {
        System.out.println("index sehifesine xos gelibsiniz");
        return "hello";
    }

    @GetMapping("/test")
    public String test() {
        System.out.println("tofiq test elesin???");
        return "tofiq-test";
    }

    @PostMapping("/test")
    public ModelAndView processForm(
            @RequestParam(name = "ad") String name,
            @RequestParam(name = "soyad") String surname,
            @RequestParam(name = "maas") BigDecimal salary
    ) {
        ModelAndView modelAndView = new ModelAndView("tofiq-test");

        modelAndView.addObject("ad", name);
        modelAndView.addObject("soyad", surname);
        modelAndView.addObject("maas", salary);

        return modelAndView;
    }
}
