package az.employee.controller;

import az.employee.domain.City;
import az.employee.domain.DataTableRequest;
import az.employee.domain.DataTableResult;
import az.employee.service.CandidateService;
import az.employee.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RequestMapping(value = {"/admin/", "/admin"})
@Controller
public class AdminController {

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private CommonService commonService;

    @GetMapping("/")
    public String index() {
        return "admin/index";
    }

    @GetMapping("/candidates")
    public String employee() {
        return "admin/hrms/candidates";
    }

    @GetMapping("getAjaxUser")
    @ResponseBody
    public DataTableResult getAjaxData(@RequestParam(name = "draw") int draw,
                                       @RequestParam(name = "start") int start,
                                       @RequestParam(name = "length") int length,
                                       @RequestParam(name = "order[0][column]") int sortColumn,
                                       @RequestParam(name = "order[0][dir]") String sortDirection,
                                       @RequestParam(name = "search[value]") String searchValue){
        System.out.println("start");
        DataTableRequest dataTableRequest = new DataTableRequest();
        dataTableRequest.setDraw(draw);
        dataTableRequest.setStart(start);
        dataTableRequest.setLength(length);
        dataTableRequest.setSortColumn(sortColumn);
        dataTableRequest.setSortDirection(sortDirection);
        dataTableRequest.setFilter(searchValue);

        return candidateService.getDataTableResponse(dataTableRequest);
    }

    @GetMapping("/addCandidate")
    public ModelAndView addCandidateIndexPage() {
        ModelAndView modelAndView = new ModelAndView("/admin/hrms/add-candidate-new");
        modelAndView.addObject("countryList", commonService.getCountryList());
        return modelAndView;
    }

    @GetMapping("/getCityList")
    @ResponseBody
    public List<City> getCountryCityList(@RequestParam(name = "country_id") long countryId) {
        return commonService.getCityList(countryId);
    }
}
