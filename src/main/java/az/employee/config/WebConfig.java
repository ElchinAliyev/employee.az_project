package az.employee.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${employee.origins}")
    private String origins;

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer
                .ignoreAcceptHeader(true)
                .favorParameter(true)
                .parameterName("tofiq")
//                .favorPathExtension(true)
//                .ignoreUnknownPathExtensions(true)
//                .useRegisteredExtensionsOnly(true)
                .mediaType("xml", MediaType.APPLICATION_XML)
                .mediaType("json", MediaType.APPLICATION_JSON)
                .mediaType("elchin", MediaType.APPLICATION_XML)
                .defaultContentType(MediaType.APPLICATION_JSON);
        ;
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        System.out.println("allowed origins = " + origins);


        registry.addMapping("/rest/**")
//                .allowedOrigins("http://localhost:8080", "http://localhost:63342")
//                .allowedOrigins("*")
                .allowedOrigins(origins.split(","))
                .allowedMethods(HttpMethod.GET.name(),
                        HttpMethod.POST.name(),
                        HttpMethod.PUT.name(),
                        HttpMethod.DELETE.name()
                );
    }
}
