package az.employee.validator;

import az.employee.domain.CandidateRegistrationForm;
import org.apache.commons.validator.GenericValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import static az.employee.constants.ValidationConstants.*;

@Component
public class CandidateRegistrationFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(CandidateRegistrationForm.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        /*
         * 1.validate first name
         *   a) not null
         *   b) not empty
         *   c) is alpha
         *   d) min/max length
         * 2.validate last name
         *   a) not null
         *   b) not empty
         *   c) is alpha
         *   d) min/max length
         * 3.validate email
         *   a) not null
         *   b) not empty
         *   c) is valid email ( regex)
         *   d) min/max length
         *   e) is duplicate email

         * 4.validate password and confirmation
         *   a) not null
         *   b) not empty
         *   c) is complex password
         *   d) min/max length
         *   e) password == confirmation
         * 5.validate accept terms
         *
         * */
        CandidateRegistrationForm form = (CandidateRegistrationForm) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "candidate.registration.firstName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "candidate.registration.lastName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "candidate.registration.email.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "candidate.registration.password.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordConfirmation", "candidate.registration.passwordConfirm.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "accept", "candidate.alma");

        if(!errors.hasErrors()) {

            if(!GenericValidator.isInRange(form.getFirstName().length(), FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH)) {
                errors.rejectValue("firstName", "candidate.registration.firstName.length", new Object[]{FIRST_NAME_MIN_LENGTH, FIRST_NAME_MAX_LENGTH}, "min/max error");
            }

            if(!GenericValidator.isEmail(form.getEmail())) {
                errors.rejectValue("email", "candidate.registration.email.invalid");
            }
        }
    }
}
