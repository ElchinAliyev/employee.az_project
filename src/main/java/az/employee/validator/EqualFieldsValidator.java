package az.employee.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;


public class EqualFieldsValidator implements ConstraintValidator<EqualFields, Object> {

    private String field1;
    private String field2;

    @Override
    public void initialize(EqualFields constraintAnnotation) {
        field1 = constraintAnnotation.field1();
        field2 = constraintAnnotation.field2();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        try {

            Object field1Value = getFieldValue(object, field1);
            Object field2Value = getFieldValue(object, field2);
            System.out.println("field1Value"  + field1Value);
            System.out.println("field2Value"  + field2Value);
            String field1 = (String) field1Value;
            String field2 = (String) field2Value;
            if(field2Value != null && field1.equals(field2) ) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private Object getFieldValue(Object object, String fieldName) throws Exception {

        Class<?> clazz = object.getClass();
        Field passwordField = clazz.getDeclaredField(fieldName);
        passwordField.setAccessible(true);
        return passwordField.get(object);
    }
}
