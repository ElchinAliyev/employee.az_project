package az.employee.security;

import az.employee.domain.Role;
import az.employee.domain.User;
import az.employee.domain.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

import static az.employee.util.SecurityUtility.hasRole;

@Component
public class EmployeeAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();
        System.out.println("logged in user = " + user);
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        session.setAttribute("loginTime", LocalDateTime.now());

        String page = "/";
        if (hasRole(user, Role.ADMIN)) {
            page = "/admin/";
        } else if (hasRole(user, Role.COMPANY)) {
            page = "/company/";
        } else if (hasRole(user, Role.CANDIDATE)) {
            page = "/candidate/";
        }

        response.sendRedirect(request.getContextPath() + page);
    }
}
