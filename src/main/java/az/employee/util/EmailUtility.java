package az.employee.util;

public class EmailUtility {

    public static String registrationSubject() {
        return "employee.az registration";
    }

    public static String registrationEmail(String name, String surname, String link) {
        return String.format("Hormetli, %s %s.\n" +
                "Employee.az saytinda qeydiyyatdan kecdiyiniz ucun tesekkur edirik.\n" +
                "Profilinizi aktivlesdirmek ucun %s linke kecin.\n" +
                "\n" +
                "Hormetle,\n" +
                "Employee.az", name, surname, link);
    }
}
