package az.employee.domain;

import java.io.Serializable;

public class Industry extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = -3410067125681196737L;
    private String name ;


    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Industry{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    public Industry() {
    }

    public Industry(long id, String name) {
        super(id);
        this.name = name;
    }

    public Industry(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
