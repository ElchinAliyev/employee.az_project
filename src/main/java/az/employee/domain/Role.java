package az.employee.domain;

import java.util.Arrays;

public enum Role {
    CANDIDATE(1), COMPANY(2), ADMIN(3);

    private int value;

    Role(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Role fromValue(int value) {
        Role type = null;

//        if(value == 1) {
//            type = CANDIDATE;
//        } else if(value == 2) {
//            type = COMPANY_REP;
//        } else if(value == 3) {
//            type = ENTREPRENEUR;
//        } else {
//            throw new RuntimeException("Invalid user type " + value);
//        }

        type = Arrays.stream(values())
                .filter(role -> role.getValue() == value)
                .findFirst().orElseThrow(() -> new RuntimeException("Invalid role " + value));

        return type;
    }
}
