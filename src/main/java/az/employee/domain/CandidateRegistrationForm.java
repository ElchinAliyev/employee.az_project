package az.employee.domain;

import az.employee.validator.EqualFields;
import az.employee.validator.NotDuplicateEmail;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@EqualFields(field1 = "password", field2 = "passwordConfirmation", message = "Sifreler ferqlidir")
public class CandidateRegistrationForm {

    @NotBlank
    @Size(min = 2, max = 50, message = "{candidate.register.name.length}")
    private String firstName;

    @NotBlank
    @Size(min = 2, max = 50, message = "{candidate.register.surname.length}")
    private String lastName;

    @NotBlank(message = "{candidate.register.email.length}")
    @Email(message = "{candidate.register.email.invalid}")
    @NotDuplicateEmail()
    private String email;

    @NotNull
    @Size(min = 8, max = 20, message = "{candidate.register.password.length}")
    private String password;

    @NotNull
    @Size(min = 8, max = 20)
    private String passwordConfirmation;

    @NotNull
    private String accept;

    public String getAccept() {
        return accept;
    }

    public void setAccept(String accept) {
        this.accept = accept;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    @Override
    public String toString() {
        return "CandidateRegistrationForm{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", passwordConfirmation='" + passwordConfirmation + '\'' +
                ", accept='" + accept + '\'' +
                '}';
    }
}
