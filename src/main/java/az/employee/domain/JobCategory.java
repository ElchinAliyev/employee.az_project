package az.employee.domain;

import java.io.Serializable;

public class JobCategory extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = -2843034964770665417L;
    private String name;

    @Override
    public String toString() {
        return "JobCategory{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    public JobCategory(long id, String name) {
        super(id);
        this.name = name;
    }

    public JobCategory() {
        this(0, "");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
