package az.employee.domain;

import az.employee.validator.NotDuplicateEmail;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class User implements Serializable {
    private static final long serialVersionUID = -5118201659592786858L;

    private long id;

    @NotBlank
    @Size(min = 2, max = 20, message = "{candidate.register.name.length}")
    private String name;

    @NotBlank
    @Size(min = 2, max = 20, message = "{candidate.register.surname.length}")
    private String surname;
    private List<Role> roleList;

    @Size(min = 6, max = 200, message = "{candidate.register.email.length}")
    @NotBlank(message = "{candidate.register.email.invalid}")
    @NotDuplicateEmail
    private String email;

    private String password;
    private UserStatus status;
    private LocalDateTime insertDate;
    private LocalDateTime lastUpdate;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String mobile;

    public User() {
        this.id = 0;
        this.name = "";
        this.surname = "";
        this.roleList = new ArrayList<>();
        this.status = UserStatus.PENDING;
        this.email = "";
        this.password = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", roleList=" + roleList +
                ", email='" + email + '\'' +
                ", userStatus=" + status +
                ", insertDate=" + insertDate +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
