package az.employee.domain;

import java.io.Serializable;

public class JobUser extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = 2799195482112654895L;
    private Job job;
    private User user;
    private Candidate candidate;
    private int jobId;
    private int userId;

    public JobUser() {
        this.job=new Job();
        this.user=new User();
        this.candidate =new Candidate();
        this.jobId=0;
        this.userId=0;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Candidate getCandidate() {
        return candidate;
    }

    public void setCandidate(Candidate candidate) {
        this.candidate = candidate;
    }

    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "JobUser{" +
                "job=" + job +
                ", user=" + user +
                ", candidate=" + candidate +
                ", jobId=" + jobId +
                ", userId=" + userId +
                ", id=" + id +
                ", insertDate=" + insertDate +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
