package az.employee.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PageResponse<T> implements Serializable {
    private static final long serialVersionUID = -6853765909653664643L;
    private long totalCount;
    private long filteredCount;
    private long dataCount;
    private long pageCount;
    private List<T> data;
    private long currentPage;

    public PageResponse() {
        this.totalCount = 0;
        this.filteredCount = 0;
        this.dataCount = 0;
        this.pageCount = 0;
        this.data = new ArrayList<>();
        this.currentPage = 1;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "PageResponse{" +
                "totalCount=" + totalCount +
                ", filteredCount=" + filteredCount +
                ", dataCount=" + dataCount +
                ", pageCount=" + pageCount +
                ", data=" + data +
                ", currentPage=" + currentPage +
                '}';
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getFilteredCount() {
        return filteredCount;
    }

    public void setFilteredCount(long filteredCount) {
        this.filteredCount = filteredCount;
    }

    public long getDataCount() {
        return dataCount;
    }

    public void setDataCount(long dataCount) {
        this.dataCount = dataCount;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }
}
