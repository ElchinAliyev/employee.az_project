package az.employee.domain;

import java.io.Serializable;

public class JobType extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = 5291483938647910335L;
    private String name;

    @Override
    public String toString() {
        return "JobType{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    public JobType(long id, String name) {
        super(id);
        this.name = name;
    }

    public JobType() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
