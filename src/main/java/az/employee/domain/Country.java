package az.employee.domain;

import java.io.Serializable;

public class Country extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = 4501964445014909941L;

    public Country() {
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", id=" + id +
                '}';
    }

    public Country(long id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
