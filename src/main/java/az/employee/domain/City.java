package az.employee.domain;

import java.io.Serializable;

public class City extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = 4501964445014909941L;
    private String name;

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", countryId=" + countryId +
                ", id=" + id +
                '}';
    }

    public long getCountryId() {
        return countryId;
    }

    public void setCountryId(long countryId) {
        this.countryId = countryId;
    }

    private long countryId;

    public City() {
    }

    public City(long id, String name, long countryId) {
        super(id);
        this.name = name;
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
