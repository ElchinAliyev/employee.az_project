package az.employee.domain;

import java.io.Serializable;

public class DataTableResult implements Serializable {
    private static final long serialVersionUID = -7370031905393151904L;
    private long draw;
    private long recordsTotal;
    private long recordsFiltered;
    private Object[][] data;

    public DataTableResult() {
    }

    public long getDraw() {
        return draw;
    }

    public void setDraw(long draw) {
        this.draw = draw;
    }

    public long getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(long recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public long getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(long recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public Object[][] getData() {
        return data;
    }

    public void setData(Object[][] data) {
        this.data = data;
    }
}
