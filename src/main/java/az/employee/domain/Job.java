package az.employee.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Job extends BaseDomainClass implements Serializable {
    private static final long serialVersionUID = -418105210126477276L;

    private String position;
    private String about;
    private String requirement;
    private BigDecimal minSalary;
    private BigDecimal maxSalary;
    private LocalDateTime postDate;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate deadline;
    private Address address;
    private JobType type;
    private int minExperience;
    private int maxExperience;
    private JobCategory category;
    private JobCategory parentCategory;
    private User user;
    private Company company;

    public Job() {
        this.position = "";
        this.about = "";
        this.requirement = "";
        this.minSalary = null;
        this.maxSalary = null;
        this.postDate = null;
        this.postDate = null;
        this.deadline = null;
        this.address = new Address();
        this.user = new User();
        this.company = null;
        this.minExperience = 0;
        this.maxExperience = 0;
        this.category = new JobCategory();
        this.parentCategory = new JobCategory();
        this.type = new JobType();
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public BigDecimal getMinSalary() {
        return minSalary;
    }

    public void setMinSalary(BigDecimal minSalary) {
        this.minSalary = minSalary;
    }

    public BigDecimal getMaxSalary() {
        return maxSalary;
    }

    public void setMaxSalary(BigDecimal maxSalary) {
        this.maxSalary = maxSalary;
    }

    public LocalDateTime getPostDate() {
        return postDate;
    }

    public void setPostDate(LocalDateTime postDate) {
        this.postDate = postDate;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalDate deadline) {
        this.deadline = deadline;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public int getMinExperience() {
        return minExperience;
    }

    public void setMinExperience(int minExperience) {
        this.minExperience = minExperience;
    }

    public int getMaxExperience() {
        return maxExperience;
    }

    public void setMaxExperience(int maxExperience) {
        this.maxExperience = maxExperience;
    }

    public JobCategory getCategory() {
        return category;
    }

    public void setCategory(JobCategory category) {
        this.category = category;
    }

    public JobCategory getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(JobCategory parentCategory) {
        this.parentCategory = parentCategory;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "Job{" +
                "position='" + position + '\'' +
                ", about='" + about + '\'' +
                ", requirement='" + requirement + '\'' +
                ", minSalary=" + minSalary +
                ", maxSalary=" + maxSalary +
                ", postDate=" + postDate +
                ", deadline=" + deadline +
                ", address=" + address +
                ", type=" + type +
                ", minExperience=" + minExperience +
                ", maxExperience=" + maxExperience +
                ", category=" + category +
                ", parentCategory=" + parentCategory +
                ", user=" + user +
                ", organization=" + company +
                ", id=" + id +
                ", insertDate=" + insertDate +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}