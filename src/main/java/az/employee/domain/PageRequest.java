package az.employee.domain;

import java.io.Serializable;

public class PageRequest implements Serializable {
    private static final long serialVersionUID = 2243708638607693121L;
    private long page;
    private long size;
    private String sortColumn;
    private String sortDir;
    private String filter;


    public long getPage() {
        return page;
    }

    public void setPage(long page) {
        this.page = page;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    @Override
    public String toString() {
        return "PageRequest{" +
                "page=" + page +
                ", size=" + size +
                ", sortColumn='" + sortColumn + '\'' +
                ", sortDir='" + sortDir + '\'' +
                ", filter='" + filter + '\'' +
                '}';
    }
}
