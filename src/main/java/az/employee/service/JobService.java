package az.employee.service;

import az.employee.domain.Job;
import az.employee.domain.JobCategoryCount;
import az.employee.domain.PageRequest;
import az.employee.domain.PageResponse;

import java.util.List;
import java.util.Optional;

public interface JobService {

    PageResponse<Job> getJobList(PageRequest request);

    long getAllJobCount();
    List<JobCategoryCount> getJobCategoryCountList();
    List<Job> getRecentJobs();
    long getJobCategoryCountById(long id);
    List<Job> getJobListByCategoryId(long id, long page);
    List<Job> getAllJobList(long page, long size);
    long getPageCount(long count, long size);

    Optional<Job> getJobById(long id);
    Job addJob(Job job);
    Job updateJob(Job job);
    boolean deleteJobById(long jobId);


}
