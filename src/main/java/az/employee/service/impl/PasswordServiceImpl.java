package az.employee.service.impl;

import az.employee.service.PasswordService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService {

    @Override
    public String encodePassword(String clearPassword) {
        return BCrypt.hashpw(clearPassword, BCrypt.gensalt());
    }

    @Override
    public boolean checkPassword(String clearPassword, String passwordHash) {
        return BCrypt.checkpw(clearPassword, passwordHash);
    }
}
