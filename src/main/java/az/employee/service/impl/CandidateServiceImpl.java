package az.employee.service.impl;

import az.employee.domain.*;
import az.employee.repository.CandidateRepository;
import az.employee.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    private PasswordService passwordService;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private CandidateRepository candidateRepository;

    @Value("${candidate.page.size}")
    private int pageSize;

    @Transactional
    @Override
    public Candidate register(Candidate candidate) {
        /*
        * candidate registration
        *  1.hash user password
           2.insert into user
           3.generate token
           4.insert token
           5.generate activation email
           6.insert activation email
        * */

        //  1.hash user password
        String encodedPassword = passwordService.encodePassword(candidate.getUser().getPassword());
        candidate.getUser().setPassword(encodedPassword);

        //  2.insert into user
        User user = userService.addUser(candidate.getUser());

        //   3.generate token
        Token token = tokenService.generateToken(user);
        token.setType(TokenType.ACTIVATION);

        //  4.insert token
        token = tokenService.addToken(token);

        // 5.generate activation email
        Email activationEmail = emailService.generateActivationEmail(token);

        // 6.insert activation email
        emailService.addEmail(activationEmail);

        return candidate;
    }


    @Override
    public DataTableResult getDataTableResponse(DataTableRequest request) {
        DataTableResult result = new DataTableResult();
        result.setDraw(request.getDraw());
        result.setRecordsTotal(candidateRepository.getCandidateCount());
        result.setRecordsFiltered(result.getRecordsTotal());
//        result.setRecordsFiltered(userRepository.getFilterCount(request.getFilter()));

        List<Candidate> list = candidateRepository.getCandidateListAdmin(request.getFilter(), request.getSortColumn(),request.getSortDirection(),request.getStart(),request.getLength());
        System.out.println("list = " +list);

        result.setData(new Object[list.size()][7]);

        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                result.getData()[i][0] = list.get(i).getUser().getName() + " " + list.get(i).getUser().getSurname();
                result.getData()[i][1] = list.get(i).getUser().getEmail();
                result.getData()[i][2] = list.get(i).getUser().getMobile();
                result.getData()[i][3] = list.get(i).getPosition();
                result.getData()[i][4] = list.get(i).getSalaryMin() + " - " + list.get(i).getSalaryMax();
                result.getData()[i][5] = list.get(i).getAddress().getCountry().getName() + " " + list.get(i).getAddress().getCity().getName();
                result.getData()[i][6] = String.format("  <button id=%d onclick='showCandidate(this)' data-target='#candidateModal' data-toggle='modal' type=\"button\" class=\"btn btn-icon\" title=\"View\"><i class=\"fa fa-eye\"></i></button>\n" +
                        "                                                <button  type=\"button\" class=\"btn btn-icon\" title=\"Edit\"><a href='editCandidate?id=%d'> <i class=\"fa fa-edit\"></i> </a> </button>\n" +
                        "                                                <button  type=\"button\" class=\"btn btn-icon js-sweetalert\" title=\"Delete\" data-type=\"confirm\"> <a href='deleteCandidate?id=%d' ><i class=\"fa fa-trash-o text-danger\"></i> </a> </button>",list.get(i).getId(),list.get(i).getId(),list.get(i).getId());

//                        "<a href='viewCandidate?id="+list.get(i).getId()+"'>View</a> "
//                + "<a href='editCandidate?id="+list.get(i).getId()+"'>Edit</a> "
//                + "<a href='deleteCandidate?id="+list.get(i).getId()+"'>Delete</a>";

            }
        }

        System.out.println("result = " +result);
        return result;
    }

    @Override
    public List<Candidate> getCandidateList(int page) {
        if(page < 1) {
            page = 1;
        }
        int offset = (page - 1) * pageSize;
        return candidateRepository.getCandidateList(offset, pageSize);
    }

    @Override
    public long getCandidatePageCount() {
        long candidateCount = candidateRepository.getCandidateCount();
        long pageCount = candidateCount/pageSize;
        if(candidateCount % pageSize > 0) {
            pageCount++;
        }
        return pageCount;
    }

    @Override
    public Optional<Candidate> getCandidateById(long id) {

//        Optional<Candidate> optionalCandidate = Optional.empty();
        Optional<Candidate> optionalCandidate = candidateRepository.getCandidateById(id);

        if(optionalCandidate.isPresent()) {
            Candidate candidate = optionalCandidate.get();
            // todo
            // is tecrubesi
            // tehsil yerler
            // dil bilikleri
            // texniki bilikler
            // sertifikatlar
            // tag list

            //is yerleri
            candidate.setJobHistoryList(candidateRepository.getJobHistoryList(candidate.getId()));

            //tehsil yerleri
            candidate.setEducationList(candidateRepository.getEducationList(candidate.getId()));

            //sertifikatlar
            candidate.setCertificateList(candidateRepository.getCertificateList(candidate.getId()));

            //dil bilikleri
            candidate.setLanguageSkillList(candidateRepository.getLanguageSkillList(candidate.getId()));

            //diger bilikler
            candidate.setSkillList(candidateRepository.getSkillList(candidate.getId()));

            //tag ler
            candidate.setTagList(candidateRepository.getTagList(candidate.getId()));
        }
        return optionalCandidate;
    }

    @Override
    public List<JobHistory> getJobHistoryList(long candidateId) {
        return candidateRepository.getJobHistoryList(candidateId);
    }

    @Transactional
    @Override
    public JobHistory addJobHistory(JobHistory jobHistory) {
        return candidateRepository.addJobHistory(jobHistory);
    }

    @Override
    public Optional<JobHistory> getJobHistoryById(long id) {
        return candidateRepository.getJobHistoryById(id);
    }

    @Transactional
    @Override
    public JobHistory updateJobHistory(JobHistory jobHistory) {
        return candidateRepository.updateJobHistory(jobHistory);
    }

    @Transactional
    @Override
    public void deleteJobHistory(long id) {
        candidateRepository.deleteJobHistory(id);
    }

    @Transactional
    @Override
    public void saveProfileImage(Candidate candidate) {
        candidateRepository.saveProfileImage(candidate.getId(), candidate.getProfileImage());
    }
}
