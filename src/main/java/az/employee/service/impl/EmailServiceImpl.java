package az.employee.service.impl;

import az.employee.domain.Email;
import az.employee.domain.Token;
import az.employee.repository.EmailRepository;
import az.employee.service.EmailService;
import az.employee.util.EmailUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService {

    @Autowired
    private EmailRepository emailRepository;

    @Value("${employee.activation.url}")
    private String activationUrl;

    @Override
    public Email generateActivationEmail(Token token) {
        Email email = new Email();
        email.setSubject(EmailUtility.registrationSubject());
        // todo move to config
        String link = activationUrl + "?token=" + token.getValue();
        String body = EmailUtility.registrationEmail(
                token.getUser().getName(),
                token.getUser().getSurname(),
                link);
        email.setBody(body);
        System.out.println("activation email = " + email);
        return email;
    }

    @Override
    public Email addEmail(Email email) {
        System.out.println("add email " + email);
        return emailRepository.addEmail(email);
    }

    @Override
    public boolean isDuplicate(String email) {
        return emailRepository.isDuplicate(email);
    }
}
