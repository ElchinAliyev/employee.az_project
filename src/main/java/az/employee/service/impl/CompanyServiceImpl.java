package az.employee.service.impl;

import az.employee.domain.*;
import az.employee.repository.CompanyRepository;
import az.employee.domain.*;
import az.employee.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CompanyServiceImpl implements CompanyService {

    @Value("${jobs.page.length}")
    private long pageLength;

    @Value("8")
    private int jobUserpageSize;

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Optional<Company> getCompanyByUserId(long userId) {
        return companyRepository.getCompanyByUserId(userId);
    }

    @Override
    public long getCompanyJobCount(long id) {
       return companyRepository.getCompanyJobCount(id);
    }

    @Override
    public long getPageCount(long id) {
        long jobCount = companyRepository.getCompanyJobCount(id);
        long pageCount = jobCount / pageLength;
        System.out.println("jobCount " + jobCount);
        System.out.println("pageLength " + pageLength);
        if (jobCount%pageLength!=0){
            pageCount++;
            System.out.println("Qalig :" +jobCount%pageLength);
        }
        System.out.println("pageCount " + pageCount);
        return pageCount;
    }

    @Override
    public List<Job> getCompanyJobListByPage(long page, long companyId) {
        long offset = 0 ;
        if (page<=1){

        }
        else {
            offset = (page-1) * pageLength;
        }
       return companyRepository.getCompanyJobListByPage(offset,pageLength,companyId);
    }



    @Override
    public void insertJob(Job job, HttpSession session) {
       User user = (User) session.getAttribute("user");
       Company company = (Company) session.getAttribute("company");
       job.setUser(user);
       job.setCompany(company);
        System.out.println(job);
       companyRepository.insertJob(job);
    }

    @Override
    public HashMap getCountryMap() {
        HashMap hashMap = new HashMap();
        List<Country> list =  companyRepository.getCountryList();
        for (Country c : list) {
            hashMap.put(c.getId(),c.getName());
        }
        return hashMap;
    }

    @Override
    public HashMap getJobTypeMap() {
        HashMap hashMap = new HashMap();
       List<JobType> list = companyRepository.getJobTypeList();
       for(JobType jobType : list){
           hashMap.put(jobType.getId(),jobType.getName());
       }
        return hashMap;
    }

    @Override
    public HashMap getJobCategoryMap() {
        HashMap hashMap = new HashMap();
      List<JobCategory> jobCategoryList =  companyRepository.getJobCategoryList();

      for(JobCategory jc : jobCategoryList){
          hashMap.put(jc.getId(),jc.getName());
      }
        return hashMap;
    }

    @Override
    public HashMap getCityMap() {
        HashMap hashMap = new HashMap();
        List<City> list = companyRepository.getCityList();

        for (City c : list){
            hashMap.put(c.getId(),c.getName());
        }

        return hashMap;
    }

    @Override
    public DataTableResult getCompanyJobDataTableResult(DataTableRequest dataTableRequest, HttpSession session) {

        DataTableResult dataTableResult = new DataTableResult();
        dataTableResult.setDraw(dataTableRequest.getDraw());

        if (session.getAttribute("company")!=null) {
            Company company = (Company) session.getAttribute("company");
          List<Job> list = companyRepository.getCompanyJobListWithAjax(dataTableRequest,company.getId());
            System.out.println(list);
            Object[][] data = new Object[list.size()][6];

            //todo ajax links
             String link = "<a  href=\"jobdetail?jobId=%s\">View</a><a href=\"editvacancy?jobId=%s\">Edit</a>" +
                     "<a class=\"delete\" href=\"deletevacancy?jobId=%s\">Delete</a>";

            for (int i = 0; i < data.length; i++) {
                data[i][0]=list.get(i).getUser().getName();
                data[i][1] = list.get(i).getPosition();
                data[i][2] = list.get(i).getCategory().getName();
                data[i][3] = list.get(i).getDeadline();
                data[i][4] = list.get(i).getAddress().getInfo();
                data[i][5] = String.format(link,list.get(i).getId(),list.get(i).getId(),list.get(i).getId());
            }
            dataTableResult.setData(data);
        long totalCount = companyRepository.getCompanyJobCount(company.getId());
        long filteredCount = companyRepository.getFilteredCompanyJobCount(dataTableRequest.getFilter(),company.getId());
            System.out.println("Filtered Count = " + filteredCount);
        dataTableResult.setRecordsFiltered(filteredCount);
            dataTableResult.setRecordsTotal(totalCount);
        }
        else {
            throw new RuntimeException("company tapilmadi");
        }
        return dataTableResult;
    }

    @Override
    public Job getJobByCompanyIdAndJobId(long jobId, long companyId) {
Job job = null;
      List<Job> jobList =  companyRepository.getJobByCompanyIdAndJobId(jobId,companyId);
      if (jobList.get(0)!=null){
          job = jobList.get(0);
      }
      else {
          System.out.println("Yanlis jobId and companyId !!");
          throw new RuntimeException("Bele  jobId ve CompanyId-e uygun is tapilmadi !");

      }

      return job;
    }

    @Override
    public void editVacancyByJob(Job job) {
        companyRepository.editVacancyByJob(job);
    }

    @Override
    public HashMap getIndustryMap() {
        HashMap hashMap = new HashMap();
       List<Industry> industryList = companyRepository.getIndustryList();
       for(Industry i : industryList){
           hashMap.put(i.getId(),i.getName());
       }
        return hashMap;
    }

    @Override
    public Company getCompanyByCompanyId(long companyId) {
     List<Company> companyList =  companyRepository.getSingleCompanyByCompanyId(companyId);
        Company company = null;
     if (companyList.get(0)!=null){
          company = companyList.get(0);
     }
     else {
         throw new RuntimeException("Company Tapilmadi !");
     }
        return company;
    }

    @Override
    public void updateCompany(Company company) {
        companyRepository.updateCompany(company);
    }

    @Override
    public void deleteJobByIdAndCompanyId(long jobId, HttpSession session) {
        Company company = (Company)session.getAttribute("company");

        companyRepository.deleteJobByIdAndCompanyId(jobId,company.getId());
    }

    @Override
    public List<JobUser> getCandidateNJobList(int page, long userId) {
        if(page < 1) {
            page = 1;
        }
        int offset = (page - 1) * jobUserpageSize;
        return companyRepository.getCandidateNJobList(offset, jobUserpageSize, userId);
    }


    @Override
    public long getCandidateNJobPageCount(long userId) {
        long jobUserCount = companyRepository.getCandidateNJobPageCount(userId);

        long pageCount = jobUserCount/jobUserpageSize;
        if(jobUserCount  % jobUserpageSize> 0) {
            pageCount++;
        }
        return pageCount;
    }

    @Override
    public long getIdFromJobUser(long jobId, long userId) {
        return companyRepository.getIdFromJobUser(jobId, userId);
    }


    @Override
    public void deleteCandidateApp(long id) {
        companyRepository.deleteCandidateApp(id);
    }

    @Override
    public long appliedJobCount(long userId){
        long jobUserCount = companyRepository.getCandidateNJobPageCount(userId);
        return jobUserCount;
    }
    @Override
    public DataTableResult dataResult(DataTableRequest request, HttpSession session, int start, int length) {
        User user = (User) session.getAttribute("user");
        long userId=user.getId();

        DataTableResult result = new DataTableResult();
        result.setDraw(request.getDraw());
        result.setRecordsTotal(companyRepository.CanAppComforAjaxCount(userId));

        result.setRecordsFiltered(companyRepository.CanAppComforAjaxFilteredCount(userId,request.getFilter()));

        Map<Integer,String> columnMap = new HashMap<>();
        columnMap.put(0,"name");
        columnMap.put(1,"surname");
        columnMap.put(2,"phone");
        columnMap.put(3,"mobile");
        columnMap.put(4,"info");
        columnMap.put(5,"cityName");

        List<Candidate> canAppList = companyRepository.getCanAppList(userId, start,
                length, request.getFilter(),
                columnMap, request);
        result.setData(new Object[canAppList.size()][8]);

        for (int i = 0; i < canAppList.size(); i++) {
            result.getData()[i][0]=canAppList.get(i).getUser().getName();
            result.getData()[i][1]=canAppList.get(i).getUser().getSurname();
            result.getData()[i][2]=canAppList.get(i).getUser().getEmail();
            result.getData()[i][3]=canAppList.get(i).getUser().getMobile();
            result.getData()[i][4]=canAppList.get(i).getUser().getPhone();
            result.getData()[i][5]=canAppList.get(i).getAddress().getInfo();
            result.getData()[i][6]=canAppList.get(i).getAddress().getCity().getName();
            /* result.getData()[i][7]=canAppList.get(i).getAddress().getCountry().getName();*/
            result.getData()[i][7] =String.format(" <a class=\"btn btn-sm btn-link\" href=\"viewProfile?candidateId=%d\"\n" +
                            "  data-toggle=\"tooltip\" title=\"View\"><i class=\"fa fa-sign-in\"></i></a>\n" +
                            "  <a id=\"deletee\" class=\"btn btn-sm btn-link hidden-xs js-sweetalert\"\n" +
                            "  data-type=\"confirm\" href=\"deleteCandidate?userId=%d\"\n" +
                            "  data-toggle=\"tooltip\" title=\"Delete\"><i class=\"fa fa-trash\"></i></a>",
                    canAppList.get(i).getId(),canAppList.get(i).getUser().getId());
        }

        return result;
    }
    @Override
    public void deleteCandidateAllApps(long userId) {
        companyRepository.deleteCandidateAllApps(userId);
    }
}
