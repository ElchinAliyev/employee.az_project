package az.employee.service;

public interface PasswordService {

    String encodePassword(String clearPassword);
    boolean checkPassword(String clearPassword, String passwordHash);
}
