package az.employee.rest;

import az.employee.domain.Candidate;
import az.employee.domain.JobHistory;
import az.employee.exception.ResourceDeleteException;
import az.employee.service.CandidateService;
import az.employee.service.FileStorageService;
import az.employee.util.FileUtility;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@RequestMapping("/rest/candidates/")
@RestController
public class CandidateRestController {

    @Autowired
    private CandidateService candidateService;

    @Autowired
    private FileStorageService fileStorageService;

    @ApiOperation(value = "get candidate by id",
        notes = "Get candidate by id",
            response = Candidate.class
    )
    @GetMapping("/{id}")
    public Candidate getCandidateById(@PathVariable(name = "id") long candidateId) {
        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
        if (optionalCandidate.isPresent()) {
            return optionalCandidate.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidate with id " + candidateId + " not found!");
        }
    }

    @GetMapping("/{candidateId}/job-history/")
    public List<JobHistory> getCandidateJobHistory(
            @PathVariable(name = "candidateId") long candidateId) {

        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
        if (optionalCandidate.isPresent()) {
            return candidateService.getJobHistoryList(candidateId);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Candidate id %s not found", candidateId));
        }
    }

    @PostMapping("/{candidateId}/job-history/")
    public JobHistory addJobHistory(
            @PathVariable(name = "candidateId") long candidateId,
            @RequestBody JobHistory jobHistory) {

        System.out.println("new job history = " + jobHistory);
        return candidateService.addJobHistory(jobHistory);
    }

    @PutMapping("/{candidateId}/job-history/{jobHistoryId}")
    public JobHistory updateJobHistory(
            @PathVariable(name = "candidateId") long candidateId,
            @PathVariable(name = "jobHistoryId") long jobHistoryId,
            @RequestBody JobHistory jobHistory) {

        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
        if (optionalCandidate.isPresent()) {

            Optional<JobHistory> optionalJobHistory = candidateService.getJobHistoryById(jobHistoryId);
            if (optionalJobHistory.isPresent()) {
                // update
                JobHistory jobHistoryFromDB = optionalJobHistory.get();
                if (jobHistoryFromDB.getCandidateId() == candidateId) {
                    System.out.println("job history found, update ");
                    return candidateService.updateJobHistory(jobHistory);
                } else {
                    throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                            "Job history with id " + jobHistoryId + " does not belong to candidate " + candidateId);
                }
            } else {
                // add
                jobHistory.setCandidateId(candidateId);
                System.out.println("job history not found, add");
                return candidateService.addJobHistory(jobHistory);
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Candidate id %s not found", candidateId));
        }

    }


    @DeleteMapping("/{candidateId}/job-history/{jobHistoryId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteJobHistory(
            @PathVariable(name = "candidateId") long candidateId,
            @PathVariable(name = "jobHistoryId") long jobHistoryId) {

        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
        if (optionalCandidate.isPresent()) {

            try {
                candidateService.deleteJobHistory(jobHistoryId);
            } catch (ResourceDeleteException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Job History with id " + jobHistoryId + " not found");
            } catch (Exception e) {
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format("Candidate id %s not found", candidateId));
        }
    }

    @PostMapping("/")
    public Candidate addCandidate(@RequestBody @Validated Candidate candidate) {
        System.out.println("rest add candidate = " + candidate);
        // todo implement add candidate
        Random random = new Random();
        candidate.setId(random.nextLong());
        candidate.getUser().setName(candidate.getUser().getName() + "-new");
        candidate.getUser().setSurname(candidate.getUser().getSurname() + "-new");

        System.out.println("rest successfully added new candidate = " + candidate);
        return candidate;
    }

    @ApiOperation(value = "upload candidate profile image",
        notes = "Multi part file upload candidate profile"
    )
    @PostMapping("/{candidateId}/profile/image")
    public void uploadProfileImage(
            @PathVariable(name = "candidateId") long candidateId,
            @ApiParam(name = "image", type = "multi part file upload")
            @RequestParam(name = "image") MultipartFile image
    ) {

        Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
        if (optionalCandidate.isPresent()) {
            Candidate candidate = optionalCandidate.get();
            String profileImage = fileStorageService.saveFile(candidateId, image);
            candidate.setProfileImage(profileImage);
            candidateService.saveProfileImage(candidate);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidate id " + candidateId + " not found");
        }

    }


    @GetMapping("/{candidateId}/profile/image")
    public ResponseEntity<Resource> getProfileImage(
            @PathVariable(name = "candidateId") long candidateId
    ) {

        boolean candidateFound = false;
        boolean profileImageFound = false;

        try {
            Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
            if (optionalCandidate.isPresent()) {
                Candidate candidate = optionalCandidate.get();
                candidateFound = true;

                if (candidate.getProfileImage() != null && !candidate.getProfileImage().isEmpty()) {
                    profileImageFound = true;
                    Resource resource = fileStorageService.getFile(candidate.getProfileImage());
                    String contentType = Files.probeContentType(resource.getFile().toPath());

                    System.out.println("resource content type = " + contentType);
                    HttpHeaders headers = new HttpHeaders();
                    headers.add("Content-Type", contentType);

                    return new ResponseEntity<>(
                            resource,
                            headers,
                            HttpStatus.OK);
                } else {
                    profileImageFound = false;
                }
            } else {
                candidateFound = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error getting candidate profile image");
        }

        if (!candidateFound) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidate id " + candidateId + " not found");
        }

        if (!profileImageFound) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Profile image not found");
        }

        return null;

    }


    @GetMapping("/{candidateId}/profile/image/download")
    public ResponseEntity<Resource> downloadProfileImage(
            @PathVariable(name = "candidateId") long candidateId
    ) {

        boolean candidateFound = false;
        boolean profileImageFound = false;

        try {
            Optional<Candidate> optionalCandidate = candidateService.getCandidateById(candidateId);
            if (optionalCandidate.isPresent()) {
                Candidate candidate = optionalCandidate.get();
                candidateFound = true;

                if (candidate.getProfileImage() != null && !candidate.getProfileImage().isEmpty()) {
                    profileImageFound = true;
                    Resource resource = fileStorageService.getFile(candidate.getProfileImage());
                    String contentType = Files.probeContentType(resource.getFile().toPath());

                    System.out.println("resource content type = " + contentType);
                    HttpHeaders headers = new HttpHeaders();
                    headers.add("Content-Type", contentType);
                    String filename = "candidate-" + candidateId + FileUtility.getFileExtension(candidate.getProfileImage());
                    headers.add("Content-Disposition", "attachment; filename=\"" +filename + "\"");

                    return new ResponseEntity<>(
                            resource,
                            headers,
                            HttpStatus.OK);
                } else {
                    profileImageFound = false;
                }
            } else {
                candidateFound = false;
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Error getting candidate profile image");
        }

        if (!candidateFound) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Candidate id " + candidateId + " not found");
        }

        if (!profileImageFound) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Profile image not found");
        }

        return null;

    }


}
