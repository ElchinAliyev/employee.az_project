<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 24.11.2019
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Edit Company</title>
</head>
<body>
<h2>Edit Company</h2>
<div>
<form:form modelAttribute="company" method="post" action="/company//editcompany">
<form:hidden path="id"/>
    Sizin Sirketin Adi : <form:input path="name"/> <br/>
    Fealiyyet Gosterdiyi Sahe : <form:select path="industry.id">
    <form:options items="${industryMap}"/>
</form:select><br/>
    Bas Ofisin Adresi : <form:input path="headOffice"/><br/>
    Yaranma Tarixi : <form:input path="createDate"/><br/>
    Iscilerin Sayi : <form:input path="numberOfEmployees"/><br/>
    Illik Gelir : <form:input path="annualRevenue"/><br/>
    Rating : <form:input path="rating"/><br/>
    About : <form:textarea path="about"/><br/>
    Phone : <form:input path="phone"/><br/>
    Mobile : <form:input path="mobile"/><br/>
    Website : <form:input path="website"/><br/>
    Email : <form:input path="email"/><br/>
    Global Sirketsiz ? : <form:radiobuttons path="global" items="${listBoolean}"></form:radiobuttons><br/>
    <input type="submit" value="submit"/><br/>
</form:form>
</div>
</body>
</html>
