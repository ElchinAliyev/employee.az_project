<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 03.12.2019
  Time: 20:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>View Job</title>
</head>
<body>
<c:choose>
    <c:when test="${job != null}" >
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="jp_job_post_main_wrapper_cont jp_job_post_grid_main_wrapper_cont">
            <div class="jp_job_post_main_wrapper jp_job_post_grid_main_wrapper">
            <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="jp_job_post_side_img">
            <img src="images/content/job_post_img1.jpg" alt="post_img" />
            </div>
            <div class="jp_job_post_right_cont jp_job_post_grid_right_cont">
            <h4>${job.position}</h4>
            <p>${job.company.name}</p>
            <ul>
            <li><i class="fa fa-cc-paypal"></i>&nbsp; ${job.minSalary} - ${job.maxSalary}</li>
            <li><i class="fa fa-map-marker"></i>${job.address.country.name} , ${job.address.city.name}</li>
            </ul>
            </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="jp_job_post_right_btn_wrapper jp_job_post_grid_right_btn_wrapper">
            <ul>
            <li><a href="#"><i class="fa fa-heart-o"></i></a></li>
            <li><a href="#">${job.type.name}</a></li>
            <li><a href="job_detail.jsp">Apply</a></li>
            </ul>
            </div>
            </div>
            </div>
            </div>
            <div class="jp_job_post_keyword_wrapper">
            <ul>
            <li><i class="fa fa-tags"></i>Keywords :</li>
            <li><a href="#">ui designer,</a></li>
            <li><a href="#">developer,</a></li>
            </ul>
            </div>
            </div>
            </div>
    </c:when>
</c:choose>
</body>
</html>
