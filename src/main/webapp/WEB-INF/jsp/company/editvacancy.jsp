<%--
  Created by IntelliJ IDEA.
  User: elchi
  Date: 24.11.2019
  Time: 20:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Add New Vacancy</title>
</head>
<body>
<h2> Add New Vacancy</h2>
<div>
<form:form modelAttribute="job" method="post" action="/company//editvacancy">
    <form:hidden path="id"/>
    <form:hidden path="address.id"/>
    Position : <form:textarea path="position" /><br/>
    <form:errors path="position"/><br/>
    About :<form:textarea path="about"/> <br/>
    <form:errors path="about"/><br/>
    Requirements: <form:textarea path="requirement" title="requirementlervi yaz"/> <br/>
    <form:errors   path="requirement"/><br/><br/>
 Job Type secin : <form:select path="type.id">
    <form:options items="${jobTypeMap}"/>
</form:select><br/><br/>
    Job Category-ni secin :
    <form:select path="category.id">
        <form:options items="${jobCategoryMap}"/>
    </form:select> <br/><br/>
    Min Experience : <form:input path="minExperience"/> <br/><br/>
    Max Experience : <form:input path="maxExperience"/><br/><br/>
    Min Salary : <form:input path="minSalary"/><br/><br/>
    Max Salary : <form:input path="maxSalary"/><br/><br/>
    Deadline : <form:input path="deadline" type="date"/>
    <br/><br/>

    Adress : <br/><div>
    Country : <form:select path="address.country.id">
    <form:options items="${countryMap}"/>
     </form:select>
   City : <form:select path="address.city.id">
        <form:options items="${cityMap}"/>
    </form:select>
    Info : <form:textarea path="address.info"/>
</div>
    <input type="submit" value="sumbit"/>
</form:form>
</div>
</body>
</html>
